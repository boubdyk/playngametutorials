package com.playncoding.playntutorialone.PlayNTutorialOne.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import com.playncoding.playntutorialone.PlayNTutorialOne.core.TutorialOne;

public class TutorialOneActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new TutorialOne());
  }
}
