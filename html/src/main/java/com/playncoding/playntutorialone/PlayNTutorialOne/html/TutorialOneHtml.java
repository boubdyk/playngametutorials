package com.playncoding.playntutorialone.PlayNTutorialOne.html;

import playn.core.PlayN;
import playn.html.HtmlGame;
import playn.html.HtmlPlatform;

import com.playncoding.playntutorialone.PlayNTutorialOne.core.TutorialOne;

public class TutorialOneHtml extends HtmlGame {

  @Override
  public void start() {
    HtmlPlatform.Config config = new HtmlPlatform.Config();
    // use config to customize the HTML platform, if needed
    HtmlPlatform platform = HtmlPlatform.register(config);
    platform.assets().setPathPrefix("PlayNTutorialOne/");
    PlayN.run(new TutorialOne());
  }
}
