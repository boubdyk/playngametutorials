package com.playncoding.playntutorialone.PlayNTutorialOne.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import com.playncoding.playntutorialone.PlayNTutorialOne.core.TutorialOne;

public class TutorialOneJava {

  public static void main(String[] args) {
    JavaPlatform.Config config = new JavaPlatform.Config();
    // use config to customize the Java platform, if needed
    JavaPlatform.register(config);
    PlayN.run(new TutorialOne());
  }
}
