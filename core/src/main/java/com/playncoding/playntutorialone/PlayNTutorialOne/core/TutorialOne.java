package com.playncoding.playntutorialone.PlayNTutorialOne.core;

//Importing required packages
import static playn.core.PlayN.*;

import java.util.Random;

import playn.core.Canvas;
import playn.core.CanvasImage;
import playn.core.Color;
import playn.core.Game;
import playn.core.Gradient;
import playn.core.Graphics;
import playn.core.Image;
import playn.core.ImageLayer;

public class TutorialOne extends Game.Default {
	
	//This is an layer which has our ball
	ImageLayer oBallLayer;

	int iBallRadius = 20;

	//The width and height of our display window/display port
	static int iScreenWidth, iScreenHeight;
	
	//call the update method every 33 milliseconds ( 1 second has 1000 ms, which means 30 times per second)
	public static final int UPDATE_RATE = 33;

	Random oRandom = new Random(System.currentTimeMillis());

	//The current X,Y position of the ball and the previous X,Y position of the ball
	private float fBallX = 200f, fBallY = 200f, fOldBallX = 200f,
			fOldBallY = 200f;

	//The speed of the ball in pixels for every 33ms (every time update is called)
	private float fBallSpeedX = 5f, fBallSpeedY = 5f;

	//The angular speed of the ball's rotation in radians, approximately 2.86 degrees
	private static float fAngularVelocity = 0.5f; // 2.86 degrees = 0.05

	//The current angle of the ball and the previous angle of the ball
	private float fAngle = 0f, fOldAngle = 0f;

	static Gradient oLinearGradient;

	public TutorialOne() {
		// this sets the rate at which we update our world's physics
    // the update method is called every UPDATE_RATE milliseconds, 1 second has 1000 milliseconds
		super(UPDATE_RATE); // call update every 33ms (30 times per second)
	}

	@Override
	//the init is called only once, when the program starts
	public void init() {
  	Graphics oGfx = graphics();

		iScreenWidth = oGfx.width();
		iScreenHeight = oGfx.height();

		// This loads the background image from our asset manager, we will talk 
    // in the next tutorial about defining resources in the asset manager.
		Image bgImage = assets().getImage("images/bg.png");
		
		// An Image is just a bitmap memory object it is not displayed anywhere
    // to actually display it we need to create an ImageLayer which holds this Image
    // a layer supports various transformations, you can move it, rotate it, resize it, scale it etc.
		ImageLayer bgLayer = oGfx.createImageLayer(bgImage);
		
  	// To display the layer we need to add it to our rootLayer - this is what is being displayed.
    // A layer can be of many types, some layers can contain other layers
		oGfx.rootLayer().add(bgLayer);

		// CanvasImage is an Image object that we can draw on, in this case we will 
    // draw a ball with a radius of iBallRadius 
		CanvasImage oBallImage;
		
		// We need to define the size of the CanvasImage, in our case it would be 
    // 2 iBallRadius high and 2 iBallRadius wide.
		oBallImage = oGfx.createImage(iBallRadius * 2, iBallRadius * 2);

		// Canvas is the actual object on which we can draw, it lives inside the CanvasImage
		Canvas oCanvas = oBallImage.canvas();

		
		// This creates a linear gradient between two random colors
    // We will use it to fill our ball 
		oLinearGradient = oGfx.createLinearGradient(0, 0, 0, iBallRadius, new int[] {
			Color.rgb(oRandom.nextInt(255), 
				oRandom.nextInt(255),	
				oRandom.nextInt(255)),
			Color.rgb(oRandom.nextInt(255), 
				oRandom.nextInt(255),
				oRandom.nextInt(255)) }, 
			new float[] { 0, 1 }
		);

		// set the gradient so it will be used for drawing our circle
		oCanvas.setFillGradient(oLinearGradient);

		// draw a circle in the middle of our canvas
		oCanvas.fillCircle(iBallRadius, iBallRadius, iBallRadius);

		// create an ImageLayer for our ball picture
		oBallLayer = oGfx.createImageLayer();
		
		// place the picture of the ball in the layer	
		oBallLayer.setImage(oBallImage);
		
		// setDepthdefines the order of how layers are drawn, in a grouplayer layers are 
    // drawn from smaller depth to higher depth. This is important if we want to 
    // draw something on top of something else
		oBallLayer.setDepth(1);
		
		// setOrigin defines a point on the layer at which transformations apply. (move, scale, rotate)
    // in our case we are using a rotation transformation and wand the ball to spin around it's center
    // the calculation places the origin at the center of the image and thus at the center of our circle
		oBallLayer.setOrigin(oBallImage.width() / 2f, oBallImage.height() / 2f);
		
		// add the layer to the rootLayer to be drawn on every frame   
		oGfx.rootLayer().add(oBallLayer);

	}

	@Override
	//update is called by the PlayN frame work every UPDATE_RATE ms
	// in this method we need to perform calculations on the physical location of the objects in our world
	// we will store the previous location and angle of our ball for use in drawing the shape
	public void update(int delta) {
		// save old values for interpolation
		fOldBallX = fBallX;
		fOldBallY = fBallY;
		fOldAngle = fAngle;
		
		// Rotation circle
		float vx = (float) (Math.cos(fAngle) * iBallRadius);
		float vy = (float) (Math.sin(fAngle) * iBallRadius);
		
		// Linear rotation + circle rotation
		// fBallX and fBallY represent the location of the center of our ball
    // We add the speed on the X axis to our X location, 
    // in order to calculate our next position on the X axis
		fBallX += fBallSpeedX;
		fBallY += fBallSpeedY;
		fBallX += vx;
		fBallY += vy;
		
		// If the ball touched or exited our viewing port 
    // we want to change the direction of our movement on that axis. 
    // Changing the direction causes the ball to move away from the boundary
 
    // Calculation of the ball exiting or touching the screen on the X axis.
    // By multiplying the speed by -1 we change the ball's direction and 
    // cause the ball to move in the oposite direction on subsequent frames
		if (fBallY > (iScreenHeight - 4 * iBallRadius) || fBallY < 4 * iBallRadius) {
			fBallSpeedY = -1 * fBallSpeedY;
		}

		// same calculation on the Y axis
		if (fBallX > (iScreenWidth - 4 * iBallRadius) || fBallX < 4 * iBallRadius) {
			fBallSpeedX = -1 * fBallSpeedX;
		}

		// The new rotation angle is the current angle plus the angular speed
		fAngle += fAngularVelocity;
	
		// if the new angle is more that 2 times PI we want to substract 2 PI from it and from the old angle.
    // an angle of 2 PI is the same as angle 0, so this calculation does not actually change the meaning 
    // of the angle value. This is done to keep the angle in the same range for ease of use and prevent 
    // overflowing of the floating point value after some period of time.
		if (fAngle > (2 * Math.PI)) {
			fAngle -= 2 * Math.PI;
			fOldAngle -= 2 * Math.PI;
		}
	}

	@Override
	//Animation in the game is achieved by drawing a series of frames, each with a small change, 
	// sufficiently quickly as to fool the human eye into thinking that the motion is continuous.
	// The paint method is called by the PlayN framework when it wants to paint a frame.
	// The decision to paint a frame is not directly related to the UPDATE_RATE we defined earlier
	// (30 frames per second). The paint function may be called more frequently or less frequently, 
	// but usually at some random time between two calls to update().
	// The alpha parameter is a value between 0 and 1, it represents how far along this call is 
	// between two calls to the update method.
	// Zero (0) meaning the update method was just called (and we still have 33ms until the next call) 
	// and One (1) meaning the update will be called immediately after this call ends.
	// So what should we draw?
	// Because this value fluctuates we cannot directly use fBallX and fBallY to draw our ball,
	// if we were to do this the ball jitters and jumps around and the motion would not be fluent.
	// Instead we are doing a linear interpolation between the old position of the ball and the current 
	// position of the ball based on the alpha value.
	// If the alpha value is closer to 0 the value of X and Y would be close to the old position but 
	// if the value is closer to 1 the ball will be closer to the current position.
	// Effectively we are painting the ball as it is moving between the
	// location calculated at the second to last call to update and the last call to update.
	public void paint(float alpha) {
		// calculate the proper X location, somewhere in the middle between the last two X values
		float x = (fBallX * alpha) + (fOldBallX * (1f - alpha));
		
		// calculate the proper Y location, somewhere in the middle between the last two Y values
		float y = (fBallY * alpha) + (fOldBallY * (1f - alpha));

		// this moves the image of the ball to the new location
		oBallLayer.setTranslation(x, y); //	oBallLayer.setTranslation(iScreenWidth / 2f , iScreenHeight / 2f);

		// calculate the proper angle value, somewhere in the middle between the last two angle values
		float angle = (fAngle * alpha) + (fOldAngle * (1f - alpha));

	 // this rotates the image of the ball to the new angle
		oBallLayer.setRotation(angle);

	}
}
